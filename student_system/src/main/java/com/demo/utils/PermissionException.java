package com.demo.utils;

public class PermissionException extends  RuntimeException {


    public PermissionException(String message) {
        super(message);
    }
}
